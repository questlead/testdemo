$(document).ready(function(){
	/*  Initiaize the header and footer for each page*/
	 $("head").append('<link rel="shortcut icon" href="img/favicon.ico" />');
	// _____________ Header Section ______________________ //
	var header_mobile = ' <button type="button btn-lg" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> \
						        <span class="sr-only">Toggle navigation</span> \
					                <span class="icon-bar"></span> \
							        <span class="icon-bar"></span> \
							        <span class="icon-bar"></span> \
						      </button>'
	
														
	var navigation_content ='<div class="col-md-offset-1 col-lg-offset-1 col-xs-offset-1">	\
      <ul class="nav navbar-nav">	\
        <li class="active"><a href="index.html">HOME</a></li>	\
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">ABOUT</a>	\
          <ul class="dropdown-menu">								\
            <li><a href="team.html">Team</a></li>					\
            <li><a href="publications.html">Publications</a></li>	\
            <li><a href="grants.html">Grants</a></li>				\
            <li><a href="presentations.html">Presentations</a></li>	\
            <li><a href="podcasts.html">Podcasts</a></li>			\
          </ul></li> <!-- tab end for About -->						\
        <li ><a href="#">SHOP</a></li>		\
        <li><a href="contact.html">CONTACT</a></li>		\
      </ul></div>';

      var banner_content = '<div class="col-md-12">		\
          <div class="carousel slide" id="carousel-picture">	\
            <ol class="carousel-indicators hidden-xs">	\
              <li data-slide-to="0" data-target="#carousel-picture"  class="active"></li> \
              <li data-slide-to="1" data-target="#carousel-picture"></li>	\
              <li data-slide-to="2" data-target="#carousel-picture"></li></ol>	\
            <div class="carousel-inner hidden-xs">	\
              <div class="item active"><img alt="First" src="img/banner-1.jpg"></div>	\
              <div class="item"><img alt="Second" src="img/banner-2.jpg"></div>	\
              <div class="item"><img alt="Third" src="img/banner-3.jpg"></div></div> 	\
           <div class="hidden-sm hidden-md hidden-lg"><img style="width:100%" src="img/banner-1.jpg"></div>	\
           <div class="carousel-caption"><img src="img/WWRP-logo.png"></div>	\
          </div></div>';

    var footer_content = "<div class='col-md-offset-1'>	\
      <p>Contact email: <a href='mailto:???@qut.edu.au'>???@qut.edu.au </a> for more information</p>	\
      <small>&copy; 2015 THE WOMEN'S WELLNESS RESEARCH PROGRAM | <a href='privacy.html'>PRIVACY</a> | <a href='declaimer.html'>DECLAIMER</a></small> \
    </div>";

	$("#header_mobile").html(header_mobile);					
	$("#navigation-display").html(navigation_content);
	$("#banner-display").html(banner_content);
	$("footer").html(footer_content);


	/*Initialize automatic slideshow*/
	$("#carousel-picture").carousel('cycle');

	$("#our_programs a").mouseenter(function(){
		//$(this).children("img").toggleClass("pale");
		var img = $(this).children('img');
		var title = $(this).attr('title');
		$('.program_description').text(title).css({            
            'height': (img.height()),
            'width': img.width(),
        });
        $(this).children(".program_description").show();
        //jQuery(".program_description",this).fadeIn("slow");

	}).mouseleave(function(){
		$(".program_description").hide();
            //jQuery(".program_description",this).fadeOut("slow");
        });


	/*$("#our_programs a").hover(function(){
		//$(this).children("img").toggleClass("pale");
		var img = $(this).children('img');
		$('<div />').text(' ').css({
            'height': img.height()/2,
            'width': img.width(),
            'background-color': '#000',
            'position': 'absolute',
            'top': 0,
            'left': 0,
            'opacity': 0.6
        }).bind('mouseout', function () {
            $(this).fadeOut('fast', function () {
                $(this).remove();
            });
        }).insertAfter(this).animate({
            'opacity': 0.6
        }, 'fast');

    });*/
	/*
	$("#our_programs a").bind('mouseover', function () {
		$(this).parent().css({
            position: 'relative'
        });
		var img = $(this).children('img');
		var title = $(this).attr('title');
		console.log (title);
        $('<a />').attr('href', $(this).attr('href'))
            .html($('<div />').text(title).css({
            'color': '#FFF',
            'height': (img.height() / 2),
            'width': img.width(),
            'background-color': '#000',
            'position': 'absolute',
            'bottom': 0,
            'left': 0,
            'opacity': 0.6
        }).bind('mouseout', function() {
            $(this).fadeOut('fast', function() {
                $(this).remove();
            });
        })).insertAfter(this).animate({
            'opacity': 0.6
        }, 'fast');
	});*/
})



